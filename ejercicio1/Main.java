package TP2mu0154.ejercicio1;

public class Main {
    
    public static void main(String[] args) {
        Año año1 = new Año(2024);
        Año año2 = new Año(2021);

        
        año1.imprimirSiEsBisiesto();
        System.out.println("-----------");
        año2.imprimirSiEsBisiesto();

        
    }

}
