package TP2mu0154.ejercicio1;

public class Año {
    
    private   int año;
    
    public Año (int año){
        this.año = año;
    }


    public  int getAño(){
        return año;
    }

    public boolean esBisiesto() {
        if (año % 4 == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public void imprimirSiEsBisiesto(){
        if (esBisiesto()){
                    System.out.println("El año " + año + " es bisiesto");
                } else {
                    System.out.println("El año " + año + " no es bisiesto");
                }
    }
    
    
}
