package TP2mu0154.ejercicio2;

public class Cadena {
    private String cadena;
     
    public Cadena(String cadena) {
        this.cadena = cadena;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    
    //METODOS

    //  .lengh() nos dice el tamaño del string o array
    public int  mostrarLongitud(){
        return cadena.length();
    }
    
    // toLowerCase() convierte cada char en minuscula .... toUpperCase()  en mayuscula
    public String convertirAMinusculas() {
        return cadena.toLowerCase();
    }


    // se convierte el string a minuscula o mayuscula para controlar mejor.
    // con charAt se puede enviar un entero para ubicarse en esa posicion del string
    public void contieneLetraA(){
        int cantidad = 0;
        int posicion = 0;
        for (int j = 0; j < cadena.length(); j++) {
            char c = Character.toLowerCase(cadena.charAt(j));
            if (c == 'a'){
                cantidad++;
                posicion = j + 1;
                System.out.println("Existe una vocal 'a' en la posición: " + posicion);
            } 
            
            if (j == cadena.length()-1 & cantidad == 0 ){
                System.out.println("La cadena no contiene la letra 'a'");
            }

        }
    }

    public String invertir(){
        String invertida = "";
        for (int i = cadena.length() - 1; i >= 0; i--) {
			invertida = invertida + cadena.charAt(i);
		}
		return invertida;
    }

    
    public String getSeparadaConGuiones() {
        StringBuilder resultado = new StringBuilder();
    
        for (int i = 0; i < cadena.length(); i++) {
          resultado.append(cadena.charAt(i));
          if (i < cadena.length() - 1) {
            resultado.append("-");
          }
        }
    
        return resultado.toString();
      }

    public int cantidadVocal(){
        int cantidad = 0;
        for (int j = 0; j < cadena.length(); j++) {
            char c = Character.toLowerCase(cadena.charAt(j));
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'){
                cantidad++;
            }
        }
        return cantidad;
    }




}
