package TP2mu0154.ejercicio2;

public class Main {
    
    public static void main(String[] args) {
        
        Cadena cadena1 = new Cadena("HOLA MUNDO");
        Cadena cadena2 = new Cadena("HELLO WORLD");

        System.out.println("Primera cadena: ");
        System.out.println("La longitud de la cadena es de: " + cadena1.mostrarLongitud() + " caracteres");
        System.out.println("La cadena convertida a minuscula: " + cadena1.convertirAMinusculas());
        cadena1.contieneLetraA();
        System.out.println("La cadena invertida resulta en: " + cadena1.invertir());
        System.out.println("Cada caracter separado por guion: " + cadena1.getSeparadaConGuiones());
        System.out.println("La cantidad de vocales contenida en la cadena es de: " + cadena1.cantidadVocal());

        System.out.println("-----------------------") ;

        System.out.println("Segunda cadena: ");
        System.out.println("La longitud de la cadena es de: " + cadena2.mostrarLongitud() + " caracteres");
        System.out.println("La cadena convertida a minuscula: " + cadena2.convertirAMinusculas());
        cadena2.contieneLetraA();
        System.out.println("La cadena invertida resulta en: " + cadena2.invertir());
        System.out.println("Cada caracter separado por guion: " + cadena2.getSeparadaConGuiones());
        System.out.println("La cantidad de vocales contenida en la cadena es de: " + cadena2.cantidadVocal());
    }

}
