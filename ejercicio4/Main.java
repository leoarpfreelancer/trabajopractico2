package TP2mu0154.ejercicio4;

public class Main {
    
    public static void main(String[] args) {
        
        Collection alumnos = new Collection();
        
        Alumno alu1 = new Alumno("Castro", "Jotsuha", "123", 7);
        Alumno alu2 = new Alumno("Carreño", "Mario", "124", 8);
        Alumno alu3 = new Alumno("Alcatraz", "Guillermo", "125", 7);
        Alumno alu4 = new Alumno("Diaz", "Leonel", "126", 9);
        Alumno alu5 = new Alumno("Milei", "Javier", "113", 8);

        alumnos.add(alu1);
        alumnos.add(alu2);
        alumnos.add(alu3);
        alumnos.add(alu4);
        alumnos.add(alu5);

        alumnos.imprimirAlumno("Diaz");

        System.out.println("El promedio general de notas es: " +  alumnos.promedioNota());
        
        String alumnosStr = alumnos.toString();
        System.out.println("Listado de alumnos: " + '\n' + 
        alumnosStr);

        System.out.println("La menor nota es: " + alumnos.menorNota());
        System.out.println("La mayor nota es: " + alumnos.mayorNota());

    }
}
