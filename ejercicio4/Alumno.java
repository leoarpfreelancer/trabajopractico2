package TP2mu0154.ejercicio4;

import java.util.ArrayList;

public class Alumno {
    
    private ArrayList<Alumno> alumnos;
    private String apellido;
    private String nombre;
    private String documento;
    private float nota;

    public Alumno(String apellido, String nombre, String documento, float nota){
        this.apellido = apellido;
        this.nombre = nombre;
        this.documento = documento;
        this.nota = nota;        
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

  

    @Override
    public String toString() {
        return "Persona{" + '\'' +
            "apellido: " + getApellido()+ '\'' +
            "nombre: " + getNombre() + '\'' +
            ", dni: " + getDocumento() + '\'' +
            ", nota: " + getNota()+
            '}';
    }

    
    // public float promedioNota(){
    //     float notaTotal = 0;
    //     float promedio = 0;
    //     for (Alumno alumno : alumnos) {
    //         notaTotal = notaTotal + alumno.getNota();
    //     }
    //     promedio = notaTotal / alumnos.size();
        
        
    //     return promedio;
    // }


}
