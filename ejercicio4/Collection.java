package TP2mu0154.ejercicio4;

import java.util.ArrayList;

public class Collection{

    private ArrayList<Alumno> alumnos;

    public Collection(){
        alumnos = new ArrayList<>();
    }

    public void add(Alumno alumno){
        alumnos.add(alumno);
    }

       
    public float promedioNota(){
        float notaTotal = 0;
        float promedio = 0;
        for (Alumno alumno : alumnos) {
            notaTotal = notaTotal + alumno.getNota();
            }
        promedio = notaTotal / alumnos.size();                
        return promedio;
    }

    public void imprimirAlumno(String apellido) {
        for (Alumno alumno : alumnos) {
            if(apellido == alumno.getApellido()){
                System.out.println("Alumno: " + alumno.getApellido() + ", " + alumno.getNombre() + " tiene la nota: " + alumno.getNota());
            }
        }
    }

    @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (Alumno alumno : alumnos) {
      stringBuilder.append(alumno.getApellido()).append(" - ");
      stringBuilder.append(alumno.getNombre()).append(" - ");
      stringBuilder.append(alumno.getDocumento()).append(" - ");
      stringBuilder.append(alumno.getNota()).append("\n");

    }
    return stringBuilder.toString();
  }

  public float menorNota(){
    float nota = 11;
    for (Alumno alumno : alumnos) {
        if(nota >= alumno.getNota()){
            nota = alumno.getNota();
        }
    }
    return nota;
  }

  public float mayorNota(){
    float nota = 0;
    for (Alumno alumno : alumnos) {
        if (nota <= alumno.getNota()) {
            nota = alumno.getNota();
        }
    }
    return nota;
  }



}
 
