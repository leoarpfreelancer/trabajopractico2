package TP2mu0154.ejercicio5;

import java.util.ArrayList;

public class Coleccion {

    private ArrayList<Producto> productos;

    public Coleccion(){
        productos = new ArrayList<>();
    }

    public void agregarProducto(Producto producto){
        productos.add(producto);
    }

    public String obtenerProductoPrecioMasAlto() {
        Producto productoMasCaro = null;
        float precioMaximo = Float.MIN_VALUE;
    
        for (Producto producto : productos) {
          if (producto.getPrecioVenta() > precioMaximo) {
            precioMaximo = producto.getPrecioVenta();
            productoMasCaro = producto;
          }
        }
    
        return "El producto de mayor precio de venta es: " + productoMasCaro.getNombre() + " con un costo de " + productoMasCaro.getPrecioVenta() + " pesos";
      }

   
}
