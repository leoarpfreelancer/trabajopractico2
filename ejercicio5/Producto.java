package TP2mu0154.ejercicio5;

public class Producto {
    private String nombre;
    private String codigo;
    private float precioCosto;
    private float ganancia;
    private float iva = 21/100;
    private float precioVenta;

    public Producto(String nombre, String codigo, float precioCosto, float ganancia) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.precioCosto = precioCosto;
        this.ganancia = ganancia;
        this.precioVenta = precioVenta();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public float getPrecioCosto() {
        return precioCosto;
    }

    public void setPrecioCosto(float precioCosto) {
        this.precioCosto = precioCosto;
    }

    public float getGanancia() {
        return ganancia;
    }

    public void setGanancia(float ganancia) {
        this.ganancia = ganancia;
    }

    public float getIva() {
        return iva;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    

    @Override
    public String toString(){
        return "Codigo de producto: " + codigo + '\n' + "Nombre de producto: " + nombre + '\n' + "Costo de fabrica: " + precioCosto + '\n' + "Ganancia: " + ganancia + "%" + '\n' + "IVA: " + iva + "%" + '\n' + "Precio de venta: " + precioVenta;
    }
    
    public float precioVenta(){
        float debeGanar = precioCosto*(ganancia/100);
        float precioCostoGanancia = precioCosto + debeGanar;
        float impuesto = iva * precioCostoGanancia;
        return impuesto + precioCostoGanancia ;
    }


}
