package TP2mu0154.ejercicio5;

public class Main {
    
    public static void main(String[] args) {
        
        Coleccion productos = new Coleccion();

        Producto prod1 = new Producto("3D", "1213", 100, 10);
        Producto prod2 = new Producto("Lays", "1214", 110, 15);

        productos.agregarProducto(prod1);
        productos.agregarProducto(prod2);

        System.out.println(productos.obtenerProductoPrecioMasAlto());
        System.out.println("---------" + '\n' + "---------");

        System.out.println("Producto 1: " + '\n' + prod1.toString());
        System.out.println("---------");
        System.out.println("Producto 2: " + '\n' + prod2.toString());

    }
}
