package TP2mu0154.ejercicio3;

public class Main {
    public static void main(String[] args) {
        Persona per1 = new Persona("Carlo", 45);
        Persona per2 = new Persona("Jose", 14);

        System.out.println( per1.toString() + " " + per1.puedeVotar());
        System.out.println( per2.toString() + " " + per2.puedeVotar());
    }
}
