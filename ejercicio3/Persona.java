package TP2mu0154.ejercicio3;

public class Persona {
    private String nombre;
    private int edad;

    public Persona(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }

    public int getEdad(){
        return edad;
    }
    
    public String getNombre(){
        return nombre;
    }

    @Override
    public String toString() {
        return "La persona {" +
                "nombre= '" + nombre + '\'' +
                ", edad= " + edad +
                '}';
    }


    public String puedeVotar(){
        String habilitado = "";
        if (edad >= 16){
            habilitado = "esta habilitado para votar";
            return habilitado;
        } else {
            habilitado = "no esta habilitado para votar";
            return habilitado;
        }
    }
}
